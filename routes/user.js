const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY 11-02-2021
// middleware si auth.verify - hindi magpproceed kapag failed na sa auth
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})

// For enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin == false){
		let data = {
			userId: userData.id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	} else{
		res.send('Not authorized')
	}

});

module.exports = router;

